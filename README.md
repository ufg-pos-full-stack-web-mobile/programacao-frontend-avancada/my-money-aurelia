# My Money - Aurelia

Aplicação front end escrita utilizando o [Aurélia Framework](http://aurelia.io/).

O procedimento de instalação do Aurélia pode ser visto nesse [link](http://aurelia.io/docs/build-systems/aurelia-cli#machine-setup);

## Criando um projeto com o Aurélia CLI:

Para criar um projeto com o Aurelia CLI basta utilizar o comando:

`$ au new my-money-aurelia`

Onde o 'my-money-aurelia' corresponde ao nome do projeto que deve ser criado.

A opção escolhida como linguagem do projeto foi o Typescript.

## Configurando as rotas

Para configurar as rotas, modificamos o arquivo `main.ts` adicionando a chamada ao método `router()`;

Após isso modifiquei o arquivo `app.ts` adicionando o código de configuração da primeira rota;

A primeira rota foi criada para a página inicial do projeto, a Home.

## Criando primeiro componente: A home

Criamos a primeira rota apontando para a Home, agora precisamos criar esse componente como página inicial do projeto, onde será mostrado um resumo das informações existentes no sistema.

Para criar o primeiro componente, utilizamos o comando `generate` do CLI do Aurelia:

`$ au generate component`

Esse comando faz duas perguntas:

* O nome do componente que se deseja criar. Como resposta informamos o nome `home`
* Em qual diretório desejamos salvar o componente partindo do diretório `src`. Para esse componente utilizamos o padrao, ou seja, o próprio diretório `src`.

Ao finalizar esse comando é criado o componente com um Hello World básico.

## Implementando listagem de Contas

Vamos implementar a primeira funcionalidade da aplicação que é a listagem de Contas.

Começamos criando uma rota em `app.ts`.

Uma vez configurada a rota, criamos o componente dentro do diretório `view`:

`$ au generate component contas-list`

Por fim implementamos a lista em uma tabela simples onde os dados foram gerados, inicialmente, de maneira _hard coded_;

### Criando modelo para Conta

Criamos nossa primeira entidade de modelo na pasta `models`. Esse modelo será um arquivo Typescript contendo os dados da entidade Conta. Inicialmente nossa conta possuirá apenas um identificador e um nome.

### Fazendo com que a listagem de Contas retorne valores dinamicamente do ViewModel

Em seguida fizemos com que os dados da Conta apresentado na listagem fosse retornado na tabela à partir do ViewModel de listagem de contas, o arquivo `contas-list.ts`.

Para isso importamos a entidade `Conta` em `conta-list.ts`, criamos uma propriedade `_contas` para comportar um array de Contas e criamos um construtor onde 3 contas foram inicializadas. Também criamos um método *GET* para que ele retornasse o array de Contas.

Depois modificamos o arquivo HTML `contas-list.html` para que os dados gerados na tabela fossem buscados do _view model_.

### Criando serviço responsável por buscar Contas em local diferente

Dá maneira que foi feito até o momento, onde as contas reenderizadas na view estão sendo criados no próprio view-model não é a melhor maneira. Portanto criamos um serviço responsável por buscar as contas de maneira mais inteligente.

Portanto criamos um serviço chamado `contas-service.ts` dentro do diretório `services`. Nessa classe criamos apenas um método inicialmente chamado `recuperarTodasContas()`. Movemos para ele a lógica de criar um array de contas e devolver para o _view model_.

Dessa forma foi preciso utilizar o serviço de injeção de dependência do Aurelia para fazer com que o serviço fosse injetado no _view model_ e continuasse buscando a lista de contas disponíveis na aplicação.

### Delegando ainda mais a busca de Contas

Apesar da camada de serviços ser a responsável por saber como buscar as contas para a aplicação, em uma aplicação real sabemos que esses dados estariam em um Back-End. Queremos utilizar a camada de serviços para realizar algumas lógicas de negócio, mas queremos deixar a responsabilidade de buscar os dados do local onde ele realmente existe para uma outra camada. Logo criamos uma camada chamada `connectors`. Essa camada terá a lógica de conectar com o servidor no Back-End e trazer os resultados que desejamos. Para esse código não temos um back-end implementado, portanto vamos criar uma implementação fackie para ele, inicialmente guardando os dados em um array de contas, mas posteriormente podemos implementar uma lógica um pouco melhor (um local storage, por exemplo);

No diretório `connectors` criamos a class `conta-connector.ts` com o método `getAll()` que deve buscar todas as contas existentes. Movemos para essa classe a lógica da criação das contas a aqui vamos tratar as inclusões, exclusões e pesquisas.

## Implementando criação de Contas

Vamos implementar a funcionalidade de criação de novas contas. Começamos criando uma nova rota chamada `conta` no arquivo `app.ts`.

Em seguida criamos o novo componente no diretório `view` com o CLI do Aurelia:

`$ au generate component conta`

Depois criamos um formulário onde recebemos os dados para criação da conta. O formulário possui apenas um input no momento com o nome da conta e um botão para salvar a nova conta.

Já no arquivo `conta-edit.ts` criamos um parâmetro chamado `_conta` que mantêm os dados da conta a serem salvos. Também foi criado o método `salvar()` para realizar a operação de persistir nova conta.

### Injetando serviço de Contas e persistindo nova conta.

Agora precisamos delegar para o serviço a responsabilidade de criar a nova conta na aplicação. No momento não temos regras de negócios complexas, portanto vamos apenas criar o método `criarNova()` no serviço `conta-servico.ts` e usar ele para delegar para o `conta-connector.ts` a responsabilidade de salvar a conta. Também injetamos o serviço `ContaService`
no _view model_ `conta-edit.ts`;

### Atribuindo identificadores nas contas

As contas são identficadas por identificadores, um parâmetro chamado `_id` no modelo `Conta`. Deixamos a responsabilidade de atribuíção dos ids para o conector de Conta.

Para realizar essa implementação criamos uma atríbuto contador em `conta-connector.ts` iniciando com o valor 1; A cada conta inserida esse contador manda o valor atual dele para o identificador da conta depois incrementa o valor do contador para a inserção da próxima conta.

Para garantir que esse valor está sendo corretamente preenchido, vamos implementar a função de exclusão na listagem das contas.

## Implementando a função de exclusão de Contas

Implementamos a função de exclusão das contas na aplicação. Começamos criando a função exclusão na view `contas-list.ts` que recebe como parâmetro a conta que deve ser excluída.

No HTML `conta-list.html` linkamos o comportamento do botão excluir com o método de exclusão criado no ViewModel. Criamos o serviço para exclusão, bem como implementamos o comportamento no conector de contas.


## Implementando a funcionalidade de edição de Conta

Deve ser possível editar a conta na aplicação. Ao clicar em editar, o usuário deve ser redirecionado para a view `conta-edit` e essa tela deve ser preenchida com os dados da conta que deve ser editada. A edição deve ser feita (mudar o nome até o presente momento) e o valor deve ser atualizado na lista de contas existente.

Primeiramente vamos criar uma rota para a edição. Vamos apontar ela para o mesmo componente de edição, só que ela vai receber um parâmetro opcional chamado id. Depois vamos criar o método `activate()` para pegar o ID e buscar a conta se necessário. Movemos para dentro do método `activate()` a lógica necessária para criar uma nova conta ou carregar uma pré existente.

Depois criamos o método que busca uma conta utilizando seu identificador `buscarPorId()` na camada de serviço e também na camada de conector (`findById()`). Por fim modificamos o método `save()` de `conta-connectors.ts` para validar quando for inclusão de nova conta ou atualização de uma existente, fazendo a substituição quando necessário.

Finalizamos adicionando o comportamento no botão editar na view `contas-list.html` para que ele aponte para a tela de edição da conta.

## Implementando a tela de listagem de lançamentos

Com o lançamento de contas pronto, agora é a hora de implementar a funcionalidade de lançamentos na aplicação. Vamos começar criando o modelo de lançamentos.

Com o modelo criado, vamos criar o componente de listagem de lançamentos.
