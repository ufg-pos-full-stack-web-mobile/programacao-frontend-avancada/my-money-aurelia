export class App {
  router;

  configureRouter(config, router) {
    this.router = router;
    config.title = 'My Money - Aurelia';
    config.map([
      {route: '', name: 'home', moduleId: 'home', nav: true, title: 'Página inicial'},
      {route: 'contas', name: 'contas-list', moduleId: 'view/contas-list', nav: true, title: 'Contas'},
      {route: 'conta/:id?', name: 'conta-edit', moduleId: 'view/conta-edit'},
      {route: 'lancamentos', name: 'lancamentos-list', moduleId: 'view/lancamentos-list', nav: true, title: 'Lançamentos'},
      {route: 'lancamento', name: 'lancamento-edit', moduleId: 'view/lancamento-edit'}
    ]);
  }
}
