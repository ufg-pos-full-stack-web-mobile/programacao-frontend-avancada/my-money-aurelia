import { TipoLancamento } from './tipo-lancamento';
import { Conta } from "./conta";

export class Lancamento {

  private _id: number;
  private _dia: Date;
  private _valor: number;
  private _tipo: TipoLancamento;
  private _conta: Conta;

  set id(id: number) {
    this._id = id;
  }

  get id(): number {
    return this._id;
  }

  set dia(dia: Date) {
    this._dia = dia;
  }

  get dia(): Date {
    return this._dia;
  }

  set valor(valor: number) {
    this._valor = valor;
  }

  get valor(): number {
    return this._valor;
  }

  set tipo(tipo: TipoLancamento) {
    this._tipo = tipo;
  }

  get tipo(): TipoLancamento {
    return this._tipo;
  }

  get tipoLancamento(): string {
    switch(this._tipo) {
      case TipoLancamento.ENTRADA:
        return 'Entrada';
      case TipoLancamento.SAIDA:
        return 'Saida';
      default:
        return '';   
    }
  }

  set conta(conta: Conta) {
    this._conta = conta;
  }

  get conta(): Conta {
    return this._conta;
  }

}
