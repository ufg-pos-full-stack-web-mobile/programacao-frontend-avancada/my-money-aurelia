export class Conta {

  private _id: number;
  private _nome: string;
  private _saldo: number;

  get id(): number{
    return this._id;
  }

  set id(id: number) {
    this._id = id;
  }

  get nome(): string {
    return this._nome
  }

  set nome(nome: string) {
    this._nome =  nome;
  }

  get saldo(): number {
    return this._saldo;
  }

  set saldo(saldo: number) {
    this._saldo = saldo;
  }
 
}
