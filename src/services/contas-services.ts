import { ContaConnector } from './../connectors/conta-connector';
import { inject } from 'aurelia-framework';
import { Conta } from './../models/conta';

@inject(ContaConnector)
export class ContaService {

  private _contaConnector: ContaConnector;

  constructor(contaConnector: ContaConnector) {
    this._contaConnector = contaConnector;
  }

  recuperarTodasContas(): Conta[] {
    return this._contaConnector.getAll();
  }

  salvar(conta: Conta): void {
    this._contaConnector.save(conta);
  }

  criarNova(conta: Conta): void {
    this._contaConnector.save(conta);
  }

  excluir(conta: Conta): void {
    this._contaConnector.delete(conta);
  }

  buscarPorId(id: number): Conta {
    return this._contaConnector.findById(id);
  }

}
