import { ContaService } from './contas-services';
import { Lancamento } from './../models/lancamento';
import { LancamentoConnector } from "../connectors/lancamento-connector";
import { inject } from 'aurelia-framework';
import { TipoLancamento } from '../models/tipo-lancamento';


@inject(LancamentoConnector, ContaService)
export class LancamentoService {

  private _lancamentoConnector: LancamentoConnector;
  private _contaService: ContaService;

  constructor(lancamentoConnector: LancamentoConnector, contaService: ContaService) {
    this._lancamentoConnector = lancamentoConnector;
    this._contaService = contaService;
  }

  recuperarTodosLancamentos(): Lancamento[] {
    return this._lancamentoConnector.getAll();
  }

  salvar(lancamento: Lancamento): void {
    let idConta = lancamento.conta.id;
    let conta = this._contaService.buscarPorId(idConta);

    if (conta !== null) {
      if (lancamento.tipo == TipoLancamento.SAIDA) {
        conta.saldo -= lancamento.valor;
      } else {
        conta.saldo += lancamento.valor;
      }

      this._lancamentoConnector.save(lancamento);
      this._contaService.salvar(conta);
    }
  }

}
