import { inject } from "aurelia-framework";
import { Conta } from "../models/conta";
import { ContaService } from "../services/contas-services";

@inject(ContaService)
export class ContasList {    
  private _title: string = 'Contas';
  private _contas: Conta[];
  private _contaService: ContaService;

  constructor(contaService: ContaService) {
    this._contaService = contaService;
    this._contas = this._contaService.recuperarTodasContas();
  }

  excluir(conta: Conta) {
    this._contaService.excluir(conta);
  }

  get title(): string{
    return this._title;
  }

  get contas(): Conta[] {
    return this._contas;
  }
}
