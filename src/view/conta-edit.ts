import { ContaService } from './../services/contas-services';
import { inject } from 'aurelia-framework';
import { Conta } from './../models/conta';

@inject(ContaService)
export class ContaEdit {    
  private _title: string;
  private _conta: Conta
  private _contaService: ContaService;

  constructor(contaService: ContaService) {
    this._title = 'Nova Conta';
    this._contaService = contaService;
    this._conta = new Conta();
  }

  activate(params) {
    if(params.id !== undefined) {
      let conta = this._contaService.buscarPorId(params.id);
      if(conta !== undefined) {
        this._conta = conta;
      }
    }
  }

  save() {
    this._contaService.criarNova(this._conta);
    this._conta = new Conta();
  }

  get conta(): Conta{
    return this._conta;
  }

  get title(): string {
    return this._title;
  }
}
