import { LancamentoService } from './../services/lancamentos-service';
import { Lancamento } from './../models/lancamento';
import { inject } from 'aurelia-framework';

@inject(LancamentoService)
export class LancamentosList {    
  private _title: string;
  private _lancamentos: Lancamento[];
  private _lancamentoService: LancamentoService;

  constructor(lancamentoService: LancamentoService) {
    this._title = 'Lançamentos';
    this._lancamentoService = lancamentoService;
    this._lancamentos = this._lancamentoService.recuperarTodosLancamentos();
  }

  get title(): string {
    return this._title;
  }

  get lancamentos(): Lancamento[] {
    return this._lancamentos;
  }
}
