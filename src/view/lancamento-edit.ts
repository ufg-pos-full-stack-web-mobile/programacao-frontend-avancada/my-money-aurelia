import { ContaService } from './../services/contas-services';
import { LancamentoService } from './../services/lancamentos-service';
import { inject } from 'aurelia-framework';
import { Lancamento } from './../models/lancamento';
import { Conta } from '../models/conta';
import { TipoLancamento } from '../models/tipo-lancamento';

@inject(LancamentoService, ContaService)
export class LancamentoEdit {    
  private _title: string;
  private _lancamento: Lancamento;
  private _lancamentoService: LancamentoService;
  private _tiposLancamentos: TipoLancamento[];
  private _contas: Conta[]
  private _contaService: ContaService;

  constructor(lancamentoService: LancamentoService, contaService: ContaService) {
    this._title = "Lançamento";
    this._lancamento = new Lancamento();
    this._lancamentoService = lancamentoService;
    this._contaService = contaService;
    this._contaService= contaService;

    this._tiposLancamentos = [TipoLancamento.ENTRADA, TipoLancamento.SAIDA];
    this._contas = this._contaService.recuperarTodasContas();
  }

  save() {
    this._lancamentoService.salvar(this.lancamento);
    this._lancamento = new Lancamento();
  }

  get lancamento(): Lancamento {
    return this._lancamento;
  }

  get title(): string {
    return this._title;
  }

  get tiposLancamentos(): TipoLancamento[] {
    return this._tiposLancamentos;
  }

  get contas(): Conta[] {
    return this._contas;
  }

}
