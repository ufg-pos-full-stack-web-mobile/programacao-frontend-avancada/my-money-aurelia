import { Conta } from './../models/conta';
import { singleton } from 'aurelia-framework';

export class ContaConnector {

  private _count: number = 1;
  private _contas: Conta[];
  
    constructor() {
      let conta1 = new Conta()
      conta1.nome = 'Conta 1';
      conta1.saldo = 100.0;
      conta1.id = this._count++;
  
      let conta2 = new Conta()
      conta2.nome = 'Conta 2';
      conta2.saldo = 150.0;
      conta2.id = this._count++;
  
      let conta3 = new Conta()
      conta3.nome = 'Conta 3';
      conta3.saldo = 255.5;
      conta3.id = this._count++;
  
      this._contas = [conta1, conta2, conta3];
    }
  
    getAll(): Conta[] {
      return this._contas;
    }

    save(conta: Conta): void {
      if(conta.id !== undefined) {
        let index = this._contas.findIndex(c => c.id === conta.id);
        this._contas.splice(index, 1, conta);
      } else {
        conta.id = this._count++;
        this._contas.push(conta);
      }
    }

    delete(conta: Conta): void {
      let index = this._contas.findIndex(c => c.id === conta.id);
      this._contas.splice(index, 1);
    }

    findById(id: number): Conta {
      return this._contas.find(c => c.id == id);
    }
}
