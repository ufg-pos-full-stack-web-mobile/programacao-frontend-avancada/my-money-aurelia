import { TipoLancamento } from './../models/tipo-lancamento';
import { Lancamento } from './../models/lancamento';
import { Conta } from '../models/conta';

export class LancamentoConnector {
  
  private _count: number = 1;
  private _lancamentos: Lancamento[];

  constructor() {
    let lancamento1 = new Lancamento();
    lancamento1.dia = new Date(2017, 12, 5);
    lancamento1.tipo = TipoLancamento.SAIDA;
    lancamento1.valor = 15.5;
    lancamento1.conta = new Conta();
    lancamento1.conta.id = 2;
    lancamento1.conta.nome = 'Conta 2';
    lancamento1.id = this._count++;

    let lancamento2 = new Lancamento();
    lancamento2.dia = new Date(2017, 12, 7);
    lancamento2.tipo = TipoLancamento.ENTRADA;
    lancamento2.valor = 100;
    lancamento2.conta = new Conta();
    lancamento2.conta.id = 1;
    lancamento2.conta.nome = 'Conta 1';
    lancamento2.id = this._count++;

    let lancamento3 = new Lancamento();
    lancamento3.dia = new Date(2017, 12, 10);
    lancamento3.tipo = TipoLancamento.SAIDA;
    lancamento3.valor = 15.25;
    lancamento3.conta = new Conta();
    lancamento3.conta.id = 3;
    lancamento3.conta.nome = 'Conta 3';
    lancamento3.id = this._count++;
    
    this._lancamentos = [lancamento1, lancamento2, lancamento3];
  }

  getAll(): Lancamento[] {
    return this._lancamentos;
  }

  save(lancamento: Lancamento) {
    this._lancamentos.push(lancamento);
  }

}
